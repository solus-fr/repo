Dépôt tiers Solus FR
======

## Présentation

Ceci est un dépôt tiers pour la distribution linux [Solus](https://getsol.us). Ce dépôt *n'a rien d'officiel*, il est maintenu par des utilisateurs francophones et passionnés de Solus. Il n'a donc rien à voir avec l'équipe de développement officiel de Solus.

## Utilisation

### Ancien utilisateur du dépôt solus-3rd-party de Devil505 ?

Ce dépôt remplace le dépôt devil505 qui était sur gitlab, il faut donc le retirer comme ceci:

`sudo eopkg remove-repo devil505`

### Ajouter le dépôt à eopkg

`sudo eopkg add-repo solus-fr https://framagit.org/solus-fr/repo/raw/master/packages/eopkg-index.xml.xz`
 
### Activer le dépôt

`sudo eopkg enable-repo solus-fr`

### Désactiver le dépôt

`sudo eopkg disable-repo solus-fr`

### Retirer le dépôt

`sudo eopkg remove-repo solus-fr`

## Si voulez que l'un des paquets se retrouve sur le dépôt officiel

Faites une demande *en anglais* de paquet sur <http://dev.getsol.us/> ou soutenez la demande si elle existe déjà (voir dans la liste plus bas).

Si la team officiel est intéressée, nous leur proposerons un patch.

## Liste des paquets


| Nom du paquet| Tâche ouverte sur dev.getsol.us | Commentaire
| --- | --- | ---
| aesop |  | 
| audience |  | 
| ciano |  | 
| duckietv | [~~TASK 1338~~](https://dev.getsol.us/T1338) | Rejeté  | 
| elementary-calendar |  | 
| elementary-calendar-devel |  | 
| eradio |  | 
| findfileconflicts |  | 
| geth |  | | 
| imageburner |  | 
| karim |  | 
| mattermost-desktop-bin |  |
| mednafen |  |
| melody |  |
| monilet |  | 
| news |  | 
| notejot |  | 
| palaura |  | 
| pip |  |
| protonmail-bridge-bin | | |
| qarte |  | 
| quilter |  | 
| screencast |  | 
| screenshot-tool |  |
| showmypictures |  | 
| stacer | [~~TASK 5321~~](https://dev.getsol.us/T5321) | Rejeté |
| turtl | [~~TASK 535~~](https://dev.getsol.us/T535) | Rejeté  | 
| w3m | [R3152](https://dev.getsol.us/source/w3m/) | Version mise à jour |
| wireguard | [TASK 3778](https://dev.getsol.us/T3778) |   
| wireguard-current | [TASK 3778](https://dev.getsol.us/T3778) |  | 
| wireguard-tools | [TASK 3778](https://dev.getsol.us/T3778) |  | 

## Un problème ?

Remplissez un rapport [ici](https://framagit.org/solus-fr/repo/issues)

Merci de ne pas poster sur les forum de Solus, subreddit...etc. Comme dit plus haut, ce dépôt n'est pas officiel.

## Participe

Vous pouvez nous rejoindre via notre [framateam]().
